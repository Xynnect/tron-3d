float4x4 world : WORLD;
float4x4 view : VIEW;
float4x4 projection : PROJECTION;

float AmbientIntensity = 1;
float4 AmbientColor : AMBIENT = float4(1,1,1,1);

float DiffuseIntensity = 1;
float4 DiffuseColor : DIFFUSE = float4(1,1,1,1);

float3 LightDirection;
 
texture TextureMap;
sampler2D TextureMapSampler = sampler_state 
{ 
	texture = (TextureMap); 
	AddressU = WRAP; 
	AddressV = WRAP; 
	MinFilter = LINEAR; 
	MipFilter = LINEAR; 
	MagFilter = LINEAR; 
};

texture Brush0;
sampler2D Brush0Sampler = sampler_state 
{ 
	texture = (Brush0); 
	AddressU = WRAP; 
	AddressV = WRAP; 
	MinFilter = LINEAR; 
	MipFilter = LINEAR; 
	MagFilter = LINEAR; 
};

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float3 Normal: NORMAL;
    float2 UV : TEXCOORD0;    
};
 
struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 UV : TEXCOORD0;
    float3 Normal: TEXCOORD1;
    float3 Position3D : TEXCOORD2;
    float3 LightDirection: TEXCOORD3;
};


VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;  
    	
    float4 worldPosition = mul(input.Position, world);
    float4 viewPosition = mul(worldPosition, view);
    output.Position = mul(viewPosition, projection);
    
	output.Normal = normalize(mul(input.Normal, (float3x3)world));  
	output.Position3D = mul(input.Position, world); 
	output.LightDirection = normalize(LightDirection);
    output.UV = input.UV;       
    
    return output;
}
 
float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{	
    float4 output = (float4)0;
  	float4 finalLighting = AmbientIntensity * AmbientColor  + DiffuseIntensity * DiffuseColor * saturate(dot(input.LightDirection, input.Normal));
    
    // this will remove part of the base texture in order to apply the second material
	output = (tex2D(TextureMapSampler, input.UV));

	// MBM modify
	// if the Y position is higher than -270, use the appropriate color from the second texture instead
	if (input.Position3D[1] > -500)
	{
	    output = (tex2D(Brush0Sampler, input.UV));    
	}
    output *= finalLighting;

	// MBM end modify
    return output;
    
}
 
technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}