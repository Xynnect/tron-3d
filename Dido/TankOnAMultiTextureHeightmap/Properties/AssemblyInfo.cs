using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;


//Descriptive Assembly attributes
[assembly: AssemblyTitle("3D Tron")]
[assembly: AssemblyProduct("3D Tron")]
[assembly: AssemblyDescription("Extreme game!")]
[assembly: AssemblyCompany("Peter 'Soul In The Hole' Todorov and Dian 'Professor' Jordanov")]
[assembly: AssemblyCopyright("Copyright PD")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("3.4.0.1")]

//ComVisible is false for this component.
[assembly: ComVisible(false)]
[assembly: Guid("e29d7e45-d640-436d-9ccf-8b443a393590")]

