﻿using System ;
using System.Collections.Generic ;
using System.Drawing ;
using System.Windows.Forms ;
using System.Collections.Specialized ;

namespace SnakeCSharp
{
    public class SnakeCSharpWindow : Form
    {
        private Point dir = new Point ( 0, 10 ) ;
        private static Random myRnd = new Random ( ) ;
        private LinkedList<Point> snake = new LinkedList<Point> ( ) ;
        private Timer t = new Timer ( ) ;
        

        public SnakeCSharpWindow ( )
        {
            base.ClientSize = new Size ( 640, 480 ) ;
            base.FormBorderStyle = FormBorderStyle.FixedSingle ;
            base.Name = "SnakeCSharpWindow" ;
            this.Text = "SnakeCSharp" ;
            base.Paint += new PaintEventHandler ( this.SnakeCSharpWindow_Paint ) ;
            base.KeyDown += new KeyEventHandler ( this.SnakeCSharpWindow_KeyDown ) ;
            this.t.Tick += new EventHandler ( this.t_Tick ) ;
            this.t.Start ( ) ;
            this.snake.AddFirst ( new Point ( 10, 10 ) ) ;
        }

        private void SnakeCSharpWindow_KeyDown ( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Left )
            {
                this.dir = new Point ( -10, 0 ) ;
            }
            if ( e.KeyCode == Keys.Right )
            {
                this.dir = new Point ( 10, 0 ) ;
            }
            if ( e.KeyCode == Keys.Up )
            {
                this.dir = new Point ( 0, -10 ) ;
            }
            if ( e.KeyCode == Keys.Down )
            {
                this.dir = new Point ( 0, 10 ) ;
            }
        }

        private void SnakeCSharpWindow_Paint ( object sender, PaintEventArgs e )
        {
            int num = 0 ;
            foreach ( Point point in this.snake )
            {
                e.Graphics.FillRectangle ( Brushes.Red, point.X, point.Y, 10, 10 ) ;
                if ( ( ( num != 0 ) && ( this.snake.First.Value.X == point.X ) ) &&
                     ( this.snake.First.Value.Y == point.Y ) )
                {
                    this.t.Stop ( ) ;
                }
                num++ ;
            }
        }

        private void t_Tick ( object sender, EventArgs e )
        {
            this.snake.AddFirst (
                new Point ( this.snake.First.Value.X + this.dir.X,
                           this.snake.First.Value.Y + this.dir.Y ) ) ;
            
            this.Refresh ( ) ;
        }

        private void InitializeComponent ( )
        {
            this.SuspendLayout ( ) ;
            // 
            // SnakeCSharpWindow
            // 
            this.ClientSize = new System.Drawing.Size ( 284, 262 ) ;
            this.Name = "SnakeCSharpWindow" ;
            this.Load += new System.EventHandler ( this.SnakeCSharpWindow_Load ) ;
            this.ResumeLayout ( false ) ;
        }

        private void SnakeCSharpWindow_Load ( object sender, EventArgs e )
        {
        }
    }
}